.. Sphinx Examples documentation master file, created by
   sphinx-quickstart on Tue Feb 23 16:34:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinx Examples's documentation!
===========================================

.. contents:: Contents
   :local:

API
---

.. autosummary::
    :recursive:
    :toctree: modules

    typevar_example


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
