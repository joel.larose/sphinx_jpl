"""An example of documenting TypeVar variables."""

from typing import TypeVar, Generic

class A:
    """This is the plainest class possible."""

TypeA = TypeVar('TypeA')
"""This the plainest type variable."""

TypeB = TypeVar('TypeB', covariant=True)
"""This type variable makes generics covariant."""

TypeC = TypeVar('TypeC', contravariant=True)
"""This type variable makes generics contravariant."""

TypeD = TypeVar('TypeD', int, str)
"""This type variable can only be substituted by `int` or `str`."""

TypeE = TypeVar('TypeE', bound=A)
"""This type variable is bound by class `A`"""

TypeF = TypeVar('TypeF', bound=A, covariant=True)
"""This type variable is bound by class `A` and is covariant."""

